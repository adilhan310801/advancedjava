package kz.aitu.advancedJava.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Generated;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Activity_journal")
public class Activity_journal {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String event_type;

    private String object_type;

    private long object_id;

    private long created_timestamp;

    private long created_by;

    private String message_level;

    private String message;


}
