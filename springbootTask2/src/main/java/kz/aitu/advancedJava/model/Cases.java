package kz.aitu.advancedJava.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Generated;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Cases")
public class Cases {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String volume_number;

    private String case_title_ru;

    private String case_title_kz;

    private String case_title_en;

    private long start_date;
    private long last_date;
    private long page_number;
    private boolean signature_flag_eds;
    private String signature_eds;
    private boolean nac_sending_sign;
    private  boolean delete_sign;
    private boolean limited_access_flag;
    private String hash;
    private long version;
    private String id_version;
    private boolean version_activity_sign;
    private String note;
    private long id_location;
    private long id_case_index;
    private long id_record;
    private long destruction_act;
    private long id_company_unit;
    private String case_address_blockchain;
    private long enter_date_blockchain;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;

}
