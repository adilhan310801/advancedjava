package kz.aitu.advancedJava.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Generated;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "File_binary")
public class File_binary {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String file_binary;
    private byte file_binary_byte;

}
