package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Fond;
import kz.aitu.advancedJava.repository.FondRepository;
import kz.aitu.advancedJava.service.FondService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FondController {

    private final FondService fondService;

    public FondController(FondService fondService) {
        this.fondService = fondService;
    }

    @GetMapping("/api/fonds/{fondId}")
    public ResponseEntity<?> getFond(@PathVariable Long fondId) {
        return ResponseEntity.ok(fondService.getById(fondId));
    }

    @GetMapping("/api/fonds")
    public ResponseEntity<?> getFonds() {
        return ResponseEntity.ok(fondService.getAll());
    }

    @PostMapping("/api/fonds")
    public ResponseEntity<?> saveFond(@RequestBody Fond fond) {
        return ResponseEntity.ok(fondService.create(fond));
    }

    @PutMapping("/api/fonds")
    public ResponseEntity<?> updateFond(@RequestBody Fond fond) {
        return ResponseEntity.ok(fondService.create(fond));
    }

    @DeleteMapping("/api/fonds/{fondId}")
    public void deleteFond(@PathVariable Long fondId) {
        fondService.delete(fondId);
    }
}
