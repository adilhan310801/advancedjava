package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Search_key_routing;
import kz.aitu.advancedJava.repository.Search_key_routingRepository;
import kz.aitu.advancedJava.service.Search_key_routingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Search_key_routingController {

    private final Search_key_routingService search_key_routingService;

    public Search_key_routingController(Search_key_routingService search_key_routingService) {
        this.search_key_routingService = search_key_routingService;
    }

    @GetMapping("/api/search_key_routings/{search_key_routingId}")
    public ResponseEntity<?> getSearch_key_routing(@PathVariable Long search_key_routingId) {
        return ResponseEntity.ok(search_key_routingService.getById(search_key_routingId));
    }

    @GetMapping("/api/search_key_routings")
    public ResponseEntity<?> getSearch_key_routings() {
        return ResponseEntity.ok(search_key_routingService.getAll());
    }

    @PostMapping("/api/search_key_routings")
    public ResponseEntity<?> saveSearch_key_routing(@RequestBody Search_key_routing search_key_routing) {
        return ResponseEntity.ok(search_key_routingService.create(search_key_routing));
    }

    @PutMapping("/api/search_key_routings")
    public ResponseEntity<?> updateSearch_key_routing(@RequestBody Search_key_routing search_key_routing) {
        return ResponseEntity.ok(search_key_routingService.create(search_key_routing));
    }

    @DeleteMapping("/api/search_key_routings/{search_key_routingId}")
    public void deleteSearch_key_routing(@PathVariable Long search_key_routingId) {
        search_key_routingService.delete(search_key_routingId);
    }
}
