package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Catalogs;
import kz.aitu.advancedJava.repository.CatalogsRepository;
import kz.aitu.advancedJava.service.CatalogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CatalogsController {

    private final CatalogsService catalogsService;

    public CatalogsController(CatalogsService catalogsService) {
        this.catalogsService = catalogsService;
    }

    @GetMapping("/api/catalogss/{catalogsId}")
    public ResponseEntity<?> getCatalogs(@PathVariable Long catalogsId) {
        return ResponseEntity.ok(catalogsService.getById(catalogsId));
    }

    @GetMapping("/api/catalogss")
    public ResponseEntity<?> getCatalogss() {
        return ResponseEntity.ok(catalogsService.getAll());
    }

    @PostMapping("/api/catalogss")
    public ResponseEntity<?> saveCatalogs(@RequestBody Catalogs catalogs) {
        return ResponseEntity.ok(catalogsService.create(catalogs));
    }

    @PutMapping("/api/catalogss")
    public ResponseEntity<?> updateCatalogs(@RequestBody Catalogs catalogs) {
        return ResponseEntity.ok(catalogsService.create(catalogs));
    }

    @DeleteMapping("/api/catalogss/{catalogsId}")
    public void deleteCatalogs(@PathVariable Long catalogsId) {
        catalogsService.delete(catalogsId);
    }
}
