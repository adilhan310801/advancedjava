package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.File_binary;
import kz.aitu.advancedJava.repository.File_binaryRepository;
import kz.aitu.advancedJava.service.File_binaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class File_binaryController {

    private final File_binaryService file_binaryService;

    public File_binaryController(File_binaryService file_binaryService) {
        this.file_binaryService = file_binaryService;
    }

    @GetMapping("/api/file_binarys/{file_binaryId}")
    public ResponseEntity<?> getFile_binary(@PathVariable Long file_binaryId) {
        return ResponseEntity.ok(file_binaryService.getById(file_binaryId));
    }

    @GetMapping("/api/file_binarys")
    public ResponseEntity<?> getFile_binarys() {
        return ResponseEntity.ok(file_binaryService.getAll());
    }

    @PostMapping("/api/file_binarys")
    public ResponseEntity<?> saveFile_binary(@RequestBody File_binary file_binary) {
        return ResponseEntity.ok(file_binaryService.create(file_binary));
    }

    @PutMapping("/api/file_binarys")
    public ResponseEntity<?> updateFile_binary(@RequestBody File_binary file_binary) {
        return ResponseEntity.ok(file_binaryService.create(file_binary));
    }

    @DeleteMapping("/api/file_binarys/{file_binaryId}")
    public void deleteFile_binary(@PathVariable Long file_binaryId) {
        file_binaryService.delete(file_binaryId);
    }
}
