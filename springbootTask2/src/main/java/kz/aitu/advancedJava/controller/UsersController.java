package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Users;
import kz.aitu.advancedJava.repository.UsersRepository;
import kz.aitu.advancedJava.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UsersController {

    private final UsersService usersService;

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("/api/userss/{usersId}")
    public ResponseEntity<?> getUsers(@PathVariable Long usersId) {
        return ResponseEntity.ok(usersService.getById(usersId));
    }

    @GetMapping("/api/userss")
    public ResponseEntity<?> getUserss() {
        return ResponseEntity.ok(usersService.getAll());
    }

    @PostMapping("/api/userss")
    public ResponseEntity<?> saveUsers(@RequestBody Users users) {
        return ResponseEntity.ok(usersService.create(users));
    }

    @PutMapping("/api/userss")
    public ResponseEntity<?> updateUsers(@RequestBody Users users) {
        return ResponseEntity.ok(usersService.create(users));
    }

    @DeleteMapping("/api/userss/{usersId}")
    public void deleteUsers(@PathVariable Long usersId) {
        usersService.delete(usersId);
    }
}
