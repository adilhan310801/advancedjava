package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Notification;
import kz.aitu.advancedJava.repository.NotificationRepository;
import kz.aitu.advancedJava.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class NotificationController {

    private final NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping("/api/notifications/{notificationId}")
    public ResponseEntity<?> getNotification(@PathVariable Long notificationId) {
        return ResponseEntity.ok(notificationService.getById(notificationId));
    }

    @GetMapping("/api/notifications")
    public ResponseEntity<?> getNotifications() {
        return ResponseEntity.ok(notificationService.getAll());
    }

    @PostMapping("/api/notifications")
    public ResponseEntity<?> saveNotification(@RequestBody Notification notification) {
        return ResponseEntity.ok(notificationService.create(notification));
    }

    @PutMapping("/api/notifications")
    public ResponseEntity<?> updateNotification(@RequestBody Notification notification) {
        return ResponseEntity.ok(notificationService.create(notification));
    }

    @DeleteMapping("/api/notifications/{notificationId}")
    public void deleteNotification(@PathVariable Long notificationId) {
        notificationService.delete(notificationId);
    }
}
