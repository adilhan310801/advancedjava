package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Case_index;
import kz.aitu.advancedJava.repository.Case_indexRepository;
import kz.aitu.advancedJava.service.Case_indexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Case_indexController {

    private final Case_indexService case_indexService;

    public Case_indexController(Case_indexService case_indexService) {
        this.case_indexService = case_indexService;
    }

    @GetMapping("/api/case_indexs/{case_indexId}")
    public ResponseEntity<?> getCase_index(@PathVariable Long case_indexId) {
        return ResponseEntity.ok(case_indexService.getById(case_indexId));
    }

    @GetMapping("/api/case_indexs")
    public ResponseEntity<?> getCase_indexs() {
        return ResponseEntity.ok(case_indexService.getAll());
    }

    @PostMapping("/api/case_indexs")
    public ResponseEntity<?> saveCase_index(@RequestBody Case_index case_index) {
        return ResponseEntity.ok(case_indexService.create(case_index));
    }

    @PutMapping("/api/case_indexs")
    public ResponseEntity<?> updateCase_index(@RequestBody Case_index case_index) {
        return ResponseEntity.ok(case_indexService.create(case_index));
    }

    @DeleteMapping("/api/case_indexs/{case_indexId}")
    public void deleteCase_index(@PathVariable Long case_indexId) {
        case_indexService.delete(case_indexId);
    }
}
