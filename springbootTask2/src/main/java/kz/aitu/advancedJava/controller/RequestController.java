package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Request;
import kz.aitu.advancedJava.repository.RequestRepository;
import kz.aitu.advancedJava.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RequestController {

    private final RequestService requestService;

    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }

    @GetMapping("/api/requests/{requestId}")
    public ResponseEntity<?> getRequest(@PathVariable Long requestId) {
        return ResponseEntity.ok(requestService.getById(requestId));
    }

    @GetMapping("/api/requests")
    public ResponseEntity<?> getRequests() {
        return ResponseEntity.ok(requestService.getAll());
    }

    @PostMapping("/api/requests")
    public ResponseEntity<?> saveRequest(@RequestBody Request request) {
        return ResponseEntity.ok(requestService.create(request));
    }

    @PutMapping("/api/requests")
    public ResponseEntity<?> updateRequest(@RequestBody Request request) {
        return ResponseEntity.ok(requestService.create(request));
    }

    @DeleteMapping("/api/requests/{requestId}")
    public void deleteRequest(@PathVariable Long requestId) {
        requestService.delete(requestId);
    }
}
