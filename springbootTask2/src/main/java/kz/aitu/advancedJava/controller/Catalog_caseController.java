package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Catalog_case;
import kz.aitu.advancedJava.repository.Catalog_caseRepository;
import kz.aitu.advancedJava.service.Catalog_caseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Catalog_caseController {

    private final Catalog_caseService catalog_caseService;

    public Catalog_caseController(Catalog_caseService catalog_caseService) {
        this.catalog_caseService = catalog_caseService;
    }

    @GetMapping("/api/catalog_cases/{catalog_caseId}")
    public ResponseEntity<?> getCatalog_case(@PathVariable Long catalog_caseId) {
        return ResponseEntity.ok(catalog_caseService.getById(catalog_caseId));
    }

    @GetMapping("/api/catalog_cases")
    public ResponseEntity<?> getCatalog_cases() {
        return ResponseEntity.ok(catalog_caseService.getAll());
    }

    @PostMapping("/api/catalog_cases")
    public ResponseEntity<?> saveCatalog_case(@RequestBody Catalog_case catalog_case) {
        return ResponseEntity.ok(catalog_caseService.create(catalog_case));
    }

    @PutMapping("/api/catalog_cases")
    public ResponseEntity<?> updateCatalog_case(@RequestBody Catalog_case catalog_case) {
        return ResponseEntity.ok(catalog_caseService.create(catalog_case));
    }

    @DeleteMapping("/api/catalog_cases/{catalog_caseId}")
    public void deleteCatalog_case(@PathVariable Long catalog_caseId) {
        catalog_caseService.delete(catalog_caseId);
    }
}
