package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Authorizations;
import kz.aitu.advancedJava.service.AuthorizationsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthorizationsController {

    private final AuthorizationsService authorizationsService;

    public AuthorizationsController(AuthorizationsService authorizationsService) {
        this.authorizationsService = authorizationsService;
    }

    @GetMapping("/api/authorizationss/{authorizationsId}")
    public ResponseEntity<?> getAuthorizations(@PathVariable Long authorizationsId) {
        return ResponseEntity.ok(authorizationsService.getById(authorizationsId));
    }

    @GetMapping("/api/authorizationss")
    public ResponseEntity<?> getAuthorizationss() {
        return ResponseEntity.ok(authorizationsService.getAll());
    }

    @PostMapping("/api/authorizationss")
    public ResponseEntity<?> saveAuthorizations(@RequestBody Authorizations authorizations) {
        return ResponseEntity.ok(authorizationsService.create(authorizations));
    }

    @PutMapping("/api/authorizationss")
    public ResponseEntity<?> updateAuthorizations(@RequestBody Authorizations authorizations) {
        return ResponseEntity.ok(authorizationsService.create(authorizations));
    }

    @DeleteMapping("/api/authorizationss/{authorizationsId}")
    public void deleteAuthorizations(@PathVariable Long authorizationsId) {
        authorizationsService.delete(authorizationsId);
    }
}
