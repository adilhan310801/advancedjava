package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Destruction_act;
import kz.aitu.advancedJava.repository.Destruction_actRepository;
import kz.aitu.advancedJava.service.Destruction_actService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Destruction_actController {

    private final Destruction_actService destruction_actService;

    public Destruction_actController(Destruction_actService destruction_actService) {
        this.destruction_actService = destruction_actService;
    }

    @GetMapping("/api/destruction_acts/{destruction_actId}")
    public ResponseEntity<?> getDestruction_act(@PathVariable Long destruction_actId) {
        return ResponseEntity.ok(destruction_actService.getById(destruction_actId));
    }

    @GetMapping("/api/destruction_acts")
    public ResponseEntity<?> getDestruction_acts() {
        return ResponseEntity.ok(destruction_actService.getAll());
    }

    @PostMapping("/api/destruction_acts")
    public ResponseEntity<?> saveDestruction_act(@RequestBody Destruction_act destruction_act) {
        return ResponseEntity.ok(destruction_actService.create(destruction_act));
    }

    @PutMapping("/api/destruction_acts")
    public ResponseEntity<?> updateDestruction_act(@RequestBody Destruction_act destruction_act) {
        return ResponseEntity.ok(destruction_actService.create(destruction_act));
    }

    @DeleteMapping("/api/destruction_acts/{destruction_actId}")
    public void deleteDestruction_act(@PathVariable Long destruction_actId) {
        destruction_actService.delete(destruction_actId);
    }
}
