package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Record;
import kz.aitu.advancedJava.repository.RecordRepository;
import kz.aitu.advancedJava.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RecordController {

    private final RecordService recordService;

    public RecordController(RecordService recordService) {
        this.recordService = recordService;
    }

    @GetMapping("/api/records/{recordId}")
    public ResponseEntity<?> getRecord(@PathVariable Long recordId) {
        return ResponseEntity.ok(recordService.getById(recordId));
    }

    @GetMapping("/api/records")
    public ResponseEntity<?> getRecords() {
        return ResponseEntity.ok(recordService.getAll());
    }

    @PostMapping("/api/records")
    public ResponseEntity<?> saveRecord(@RequestBody Record record) {
        return ResponseEntity.ok(recordService.create(record));
    }

    @PutMapping("/api/records")
    public ResponseEntity<?> updateRecord(@RequestBody Record record) {
        return ResponseEntity.ok(recordService.create(record));
    }

    @DeleteMapping("/api/records/{recordId}")
    public void deleteRecord(@PathVariable Long recordId) {
        recordService.delete(recordId);
    }
}
