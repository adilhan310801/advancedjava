package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.History_of_request_statuses;
import kz.aitu.advancedJava.repository.History_of_request_statusesRepository;
import kz.aitu.advancedJava.service.History_of_request_statusesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class History_of_request_statusesController {

    private final History_of_request_statusesService history_of_request_statusesService;

    public History_of_request_statusesController(History_of_request_statusesService history_of_request_statusesService) {
        this.history_of_request_statusesService = history_of_request_statusesService;
    }

    @GetMapping("/api/history_of_request_statusess/{history_of_request_statusesId}")
    public ResponseEntity<?> getHistory_of_request_statuses(@PathVariable Long history_of_request_statusesId) {
        return ResponseEntity.ok(history_of_request_statusesService.getById(history_of_request_statusesId));
    }

    @GetMapping("/api/history_of_request_statusess")
    public ResponseEntity<?> getHistory_of_request_statusess() {
        return ResponseEntity.ok(history_of_request_statusesService.getAll());
    }

    @PostMapping("/api/history_of_request_statusess")
    public ResponseEntity<?> saveHistory_of_request_statuses(@RequestBody History_of_request_statuses history_of_request_statuses) {
        return ResponseEntity.ok(history_of_request_statusesService.create(history_of_request_statuses));
    }

    @PutMapping("/api/history_of_request_statusess")
    public ResponseEntity<?> updateHistory_of_request_statuses(@RequestBody History_of_request_statuses history_of_request_statuses) {
        return ResponseEntity.ok(history_of_request_statusesService.create(history_of_request_statuses));
    }

    @DeleteMapping("/api/history_of_request_statusess/{history_of_request_statusesId}")
    public void deleteHistory_of_request_statuses(@PathVariable Long history_of_request_statusesId) {
        history_of_request_statusesService.delete(history_of_request_statusesId);
    }
}
