package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Files;
import kz.aitu.advancedJava.service.FilesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FilesController {

    private final FilesService filesService;

    public FilesController(FilesService filesService) {
        this.filesService = filesService;
    }

    @GetMapping("/api/filess/{filesId}")
    public ResponseEntity<?> getFiles(@PathVariable Long filesId) {
        return ResponseEntity.ok(filesService.getById(filesId));
    }

    @GetMapping("/api/filess")
    public ResponseEntity<?> getFiless() {
        return ResponseEntity.ok(filesService.getAll());
    }

    @PostMapping("/api/filess")
    public ResponseEntity<?> saveFiles(@RequestBody Files files) {
        return ResponseEntity.ok(filesService.create(files));
    }

    @PutMapping("/api/filess")
    public ResponseEntity<?> updateFiles(@RequestBody Files files) {
        return ResponseEntity.ok(filesService.create(files));
    }

    @DeleteMapping("/api/filess/{filesId}")
    public void deleteFiles(@PathVariable Long filesId) {
        filesService.delete(filesId);
    }
}
