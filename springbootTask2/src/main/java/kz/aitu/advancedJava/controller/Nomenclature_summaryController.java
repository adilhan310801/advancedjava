package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Nomenclature_summary;
import kz.aitu.advancedJava.repository.Nomenclature_summaryRepository;
import kz.aitu.advancedJava.service.Nomenclature_summaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Nomenclature_summaryController {

    private final Nomenclature_summaryService nomenclature_summaryService;

    public Nomenclature_summaryController(Nomenclature_summaryService nomenclature_summaryService) {
        this.nomenclature_summaryService = nomenclature_summaryService;
    }

    @GetMapping("/api/nomenclature_summarys/{nomenclature_summaryId}")
    public ResponseEntity<?> getNomenclature_summary(@PathVariable Long nomenclature_summaryId) {
        return ResponseEntity.ok(nomenclature_summaryService.getById(nomenclature_summaryId));
    }

    @GetMapping("/api/nomenclature_summarys")
    public ResponseEntity<?> getNomenclature_summarys() {
        return ResponseEntity.ok(nomenclature_summaryService.getAll());
    }

    @PostMapping("/api/nomenclature_summarys")
    public ResponseEntity<?> saveNomenclature_summary(@RequestBody Nomenclature_summary nomenclature_summary) {
        return ResponseEntity.ok(nomenclature_summaryService.create(nomenclature_summary));
    }

    @PutMapping("/api/nomenclature_summarys")
    public ResponseEntity<?> updateNomenclature_summary(@RequestBody Nomenclature_summary nomenclature_summary) {
        return ResponseEntity.ok(nomenclature_summaryService.create(nomenclature_summary));
    }

    @DeleteMapping("/api/nomenclature_summarys/{nomenclature_summaryId}")
    public void deleteNomenclature_summary(@PathVariable Long nomenclature_summaryId) {
        nomenclature_summaryService.delete(nomenclature_summaryId);
    }
}
