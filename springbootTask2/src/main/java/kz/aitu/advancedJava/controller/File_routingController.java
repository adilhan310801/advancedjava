package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.File_routing;
import kz.aitu.advancedJava.repository.File_routingRepository;
import kz.aitu.advancedJava.service.File_routingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class File_routingController {

    private final File_routingService file_routingService;

    public File_routingController(File_routingService file_routingService) {
        this.file_routingService = file_routingService;
    }

    @GetMapping("/api/file_routings/{file_routingId}")
    public ResponseEntity<?> getFile_routing(@PathVariable Long file_routingId) {
        return ResponseEntity.ok(file_routingService.getById(file_routingId));
    }

    @GetMapping("/api/file_routings")
    public ResponseEntity<?> getFile_routings() {
        return ResponseEntity.ok(file_routingService.getAll());
    }

    @PostMapping("/api/file_routings")
    public ResponseEntity<?> saveFile_routing(@RequestBody File_routing file_routing) {
        return ResponseEntity.ok(file_routingService.create(file_routing));
    }

    @PutMapping("/api/file_routings")
    public ResponseEntity<?> updateFile_routing(@RequestBody File_routing file_routing) {
        return ResponseEntity.ok(file_routingService.create(file_routing));
    }

    @DeleteMapping("/api/file_routings/{file_routingId}")
    public void deleteFile_routing(@PathVariable Long file_routingId) {
        file_routingService.delete(file_routingId);
    }
}
