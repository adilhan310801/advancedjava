package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Searchkey;
import kz.aitu.advancedJava.repository.SearchkeyRepository;
import kz.aitu.advancedJava.service.SearchkeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SearchkeyController {

    private final SearchkeyService searchkeyService;

    public SearchkeyController(SearchkeyService searchkeyService) {
        this.searchkeyService = searchkeyService;
    }

    @GetMapping("/api/searchkeys/{searchkeyId}")
    public ResponseEntity<?> getSearchkey(@PathVariable Long searchkeyId) {
        return ResponseEntity.ok(searchkeyService.getById(searchkeyId));
    }

    @GetMapping("/api/searchkeys")
    public ResponseEntity<?> getSearchkeys() {
        return ResponseEntity.ok(searchkeyService.getAll());
    }

    @PostMapping("/api/searchkeys")
    public ResponseEntity<?> saveSearchkey(@RequestBody Searchkey searchkey) {
        return ResponseEntity.ok(searchkeyService.create(searchkey));
    }

    @PutMapping("/api/searchkeys")
    public ResponseEntity<?> updateSearchkey(@RequestBody Searchkey searchkey) {
        return ResponseEntity.ok(searchkeyService.create(searchkey));
    }

    @DeleteMapping("/api/searchkeys/{searchkeyId}")
    public void deleteSearchkey(@PathVariable Long searchkeyId) {
        searchkeyService.delete(searchkeyId);
    }
}
