package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Locations;
import kz.aitu.advancedJava.repository.LocationsRepository;
import kz.aitu.advancedJava.service.LocationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LocationsController {

    private final LocationsService locationsService;

    public LocationsController(LocationsService locationsService) {
        this.locationsService = locationsService;
    }

    @GetMapping("/api/locationss/{locationsId}")
    public ResponseEntity<?> getLocations(@PathVariable Long locationsId) {
        return ResponseEntity.ok(locationsService.getById(locationsId));
    }

    @GetMapping("/api/locationss")
    public ResponseEntity<?> getLocationss() {
        return ResponseEntity.ok(locationsService.getAll());
    }

    @PostMapping("/api/locationss")
    public ResponseEntity<?> saveLocations(@RequestBody Locations locations) {
        return ResponseEntity.ok(locationsService.create(locations));
    }

    @PutMapping("/api/locationss")
    public ResponseEntity<?> updateLocations(@RequestBody Locations locations) {
        return ResponseEntity.ok(locationsService.create(locations));
    }

    @DeleteMapping("/api/locationss/{locationsId}")
    public void deleteLocations(@PathVariable Long locationsId) {
        locationsService.delete(locationsId);
    }
}
