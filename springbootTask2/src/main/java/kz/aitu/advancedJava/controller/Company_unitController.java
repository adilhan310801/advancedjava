package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Company_unit;
import kz.aitu.advancedJava.repository.Company_unitRepository;
import kz.aitu.advancedJava.service.Company_unitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Company_unitController {

    private final Company_unitService company_unitService;

    public Company_unitController(Company_unitService company_unitService) {
        this.company_unitService = company_unitService;
    }

    @GetMapping("/api/company_units/{company_unitId}")
    public ResponseEntity<?> getCompany_unit(@PathVariable Long company_unitId) {
        return ResponseEntity.ok(company_unitService.getById(company_unitId));
    }

    @GetMapping("/api/company_units")
    public ResponseEntity<?> getCompany_units() {
        return ResponseEntity.ok(company_unitService.getAll());
    }

    @PostMapping("/api/company_units")
    public ResponseEntity<?> saveCompany_unit(@RequestBody Company_unit company_unit) {
        return ResponseEntity.ok(company_unitService.create(company_unit));
    }

    @PutMapping("/api/company_units")
    public ResponseEntity<?> updateCompany_unit(@RequestBody Company_unit company_unit) {
        return ResponseEntity.ok(company_unitService.create(company_unit));
    }

    @DeleteMapping("/api/company_units/{company_unitId}")
    public void deleteCompany_unit(@PathVariable Long company_unitId) {
        company_unitService.delete(company_unitId);
    }
}
