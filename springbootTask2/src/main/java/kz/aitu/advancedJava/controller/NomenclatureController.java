package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Nomenclature;
import kz.aitu.advancedJava.repository.NomenclatureRepository;
import kz.aitu.advancedJava.service.NomenclatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class NomenclatureController {

    private final NomenclatureService nomenclatureService;

    public NomenclatureController(NomenclatureService nomenclatureService) {
        this.nomenclatureService = nomenclatureService;
    }

    @GetMapping("/api/nomenclatures/{nomenclatureId}")
    public ResponseEntity<?> getNomenclature(@PathVariable Long nomenclatureId) {
        return ResponseEntity.ok(nomenclatureService.getById(nomenclatureId));
    }

    @GetMapping("/api/nomenclatures")
    public ResponseEntity<?> getNomenclatures() {
        return ResponseEntity.ok(nomenclatureService.getAll());
    }

    @PostMapping("/api/nomenclatures")
    public ResponseEntity<?> saveNomenclature(@RequestBody Nomenclature nomenclature) {
        return ResponseEntity.ok(nomenclatureService.create(nomenclature));
    }

    @PutMapping("/api/nomenclatures")
    public ResponseEntity<?> updateNomenclature(@RequestBody Nomenclature nomenclature) {
        return ResponseEntity.ok(nomenclatureService.create(nomenclature));
    }

    @DeleteMapping("/api/nomenclatures/{nomenclatureId}")
    public void deleteNomenclature(@PathVariable Long nomenclatureId) {
        nomenclatureService.delete(nomenclatureId);
    }
}
