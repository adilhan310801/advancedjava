package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Company;
import kz.aitu.advancedJava.repository.CompanyRepository;
import kz.aitu.advancedJava.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CompanyController {

    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping("/api/companys/{companyId}")
    public ResponseEntity<?> getCompany(@PathVariable Long companyId) {
        return ResponseEntity.ok(companyService.getById(companyId));
    }

    @GetMapping("/api/companys")
    public ResponseEntity<?> getCompanys() {
        return ResponseEntity.ok(companyService.getAll());
    }

    @PostMapping("/api/companys")
    public ResponseEntity<?> saveCompany(@RequestBody Company company) {
        return ResponseEntity.ok(companyService.create(company));
    }

    @PutMapping("/api/companys")
    public ResponseEntity<?> updateCompany(@RequestBody Company company) {
        return ResponseEntity.ok(companyService.create(company));
    }

    @DeleteMapping("/api/companys/{companyId}")
    public void deleteCompany(@PathVariable Long companyId) {
        companyService.delete(companyId);
    }
}
