package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Cases;
import kz.aitu.advancedJava.repository.CasesRepository;
import kz.aitu.advancedJava.service.CasesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CasesController {

    private final CasesService casesService;

    public CasesController(CasesService casesService) {
        this.casesService = casesService;
    }

    @GetMapping("/api/casess/{casesId}")
    public ResponseEntity<?> getCases(@PathVariable Long casesId) {
        return ResponseEntity.ok(casesService.getById(casesId));
    }

    @GetMapping("/api/casess")
    public ResponseEntity<?> getCasess() {
        return ResponseEntity.ok(casesService.getAll());
    }

    @PostMapping("/api/casess")
    public ResponseEntity<?> saveCases(@RequestBody Cases cases) {
        return ResponseEntity.ok(casesService.create(cases));
    }

    @PutMapping("/api/casess")
    public ResponseEntity<?> updateCases(@RequestBody Cases cases) {
        return ResponseEntity.ok(casesService.create(cases));
    }

    @DeleteMapping("/api/casess/{casesId}")
    public void deleteCases(@PathVariable Long casesId) {
        casesService.delete(casesId);
    }
}
