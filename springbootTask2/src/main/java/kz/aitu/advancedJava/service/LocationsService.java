package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.Locations;
import kz.aitu.advancedJava.repository.LocationsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationsService {

    public final LocationsRepository locationsRepository;

    public LocationsService(LocationsRepository locationsRepository) {
        this.locationsRepository = locationsRepository;
    }

    public List<Locations> getAll() {
        return locationsRepository.findAll();
    }

    public Locations getById(Long id) {
        return locationsRepository.findById(id).orElse(null);
    }

    public Locations create(Locations locations) {
        return locationsRepository.save(locations);
    }

    public Locations update(Locations locations) {
        return locationsRepository.save(locations);
    }

    public void delete(Long id) {
        locationsRepository.deleteById(id);
    }


}
