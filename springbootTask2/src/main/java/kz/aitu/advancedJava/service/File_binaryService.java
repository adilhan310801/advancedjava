package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.File_binary;
import kz.aitu.advancedJava.repository.File_binaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class File_binaryService {

    public final File_binaryRepository file_binaryRepository;

    public File_binaryService(File_binaryRepository file_binaryRepository) {
        this.file_binaryRepository = file_binaryRepository;
    }

    public List<File_binary> getAll() {
        return file_binaryRepository.findAll();
    }

    public File_binary getById(Long id) {
        return file_binaryRepository.findById(id).orElse(null);
    }

    public File_binary create(File_binary file_binary) {
        return file_binaryRepository.save(file_binary);
    }

    public File_binary update(File_binary file_binary) {
        return file_binaryRepository.save(file_binary);
    }

    public void delete(Long id) {
        file_binaryRepository.deleteById(id);
    }


}
