package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.Destruction_act;
import kz.aitu.advancedJava.repository.Destruction_actRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Destruction_actService {

    public final Destruction_actRepository destruction_actRepository;

    public Destruction_actService(Destruction_actRepository destruction_actRepository) {
        this.destruction_actRepository = destruction_actRepository;
    }

    public List<Destruction_act> getAll() {
        return destruction_actRepository.findAll();
    }

    public Destruction_act getById(Long id) {
        return destruction_actRepository.findById(id).orElse(null);
    }

    public Destruction_act create(Destruction_act destruction_act) {
        return destruction_actRepository.save(destruction_act);
    }

    public Destruction_act update(Destruction_act destruction_act) {
        return destruction_actRepository.save(destruction_act);
    }

    public void delete(Long id) {
        destruction_actRepository.deleteById(id);
    }


}
