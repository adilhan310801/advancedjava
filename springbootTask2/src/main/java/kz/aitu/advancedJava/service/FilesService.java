package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.Files;
import kz.aitu.advancedJava.repository.FilesRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilesService {

    public final FilesRepository fileRepository;

    public FilesService(FilesRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public List<Files> getAll() {
        return fileRepository.findAll();
    }

    public Files getById(Long id) {
        return fileRepository.findById(id).orElse(null);
    }

    public Files create(Files file) {
        return fileRepository.save(file);
    }

    public Files update(Files file) {
        return fileRepository.save(file);
    }

    public void delete(Long id) {
        fileRepository.deleteById(id);
    }


}
