package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.History_of_request_statuses;
import kz.aitu.advancedJava.repository.History_of_request_statusesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class History_of_request_statusesService {

    public final History_of_request_statusesRepository History_of_request_statusesRepository;

    public History_of_request_statusesService(History_of_request_statusesRepository History_of_request_statusesRepository) {
        this.History_of_request_statusesRepository = History_of_request_statusesRepository;
    }

    public List<History_of_request_statuses> getAll() {
        return History_of_request_statusesRepository.findAll();
    }

    public History_of_request_statuses getById(Long id) {
        return History_of_request_statusesRepository.findById(id).orElse(null);
    }

    public History_of_request_statuses create(History_of_request_statuses History_of_request_statuses) {
        return History_of_request_statusesRepository.save(History_of_request_statuses);
    }

    public History_of_request_statuses update(History_of_request_statuses History_of_request_statuses) {
        return History_of_request_statusesRepository.save(History_of_request_statuses);
    }

    public void delete(Long id) {
        History_of_request_statusesRepository.deleteById(id);
    }


}
