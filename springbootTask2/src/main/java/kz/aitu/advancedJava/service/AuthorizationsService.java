package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.Authorizations;
import kz.aitu.advancedJava.repository.AuthorizationsRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorizationsService {

    public final AuthorizationsRepository authorizationRepository;

    public AuthorizationsService(AuthorizationsRepository authorizationRepository) {
        this.authorizationRepository = authorizationRepository;
    }

    public List<Authorizations> getAll() {
        return authorizationRepository.findAll();
    }

    public Authorizations getById(Long id) {
        return authorizationRepository.findById(id).orElse(null);
    }

    public Authorizations create(Authorizations authorization) {
        return authorizationRepository.save(authorization);
    }

    public Authorizations update(Authorizations authorization) {
        return authorizationRepository.save(authorization);
    }

    public void delete(Long id) {
        authorizationRepository.deleteById(id);
    }


}
