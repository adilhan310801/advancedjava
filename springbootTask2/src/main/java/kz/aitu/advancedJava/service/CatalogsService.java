package kz.aitu.advancedJava.service;

import kz.aitu.advancedJava.model.Catalogs;
import kz.aitu.advancedJava.repository.CatalogsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CatalogsService {

    public final CatalogsRepository catalogsRepository;

    public CatalogsService(CatalogsRepository catalogsRepository) {
        this.catalogsRepository = catalogsRepository;
    }

    public List<Catalogs> getAll() {
        return catalogsRepository.findAll();
    }

    public Catalogs getById(Long id) {
        return catalogsRepository.findById(id).orElse(null);
    }

    public Catalogs create(Catalogs catalogs) {
        return catalogsRepository.save(catalogs);
    }

    public Catalogs update(Catalogs catalogs) {
        return catalogsRepository.save(catalogs);
    }

    public void delete(Long id) {
        catalogsRepository.deleteById(id);
    }


}
