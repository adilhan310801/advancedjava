package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Catalogs;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CatalogsRepository extends CrudRepository<Catalogs, Long> {


    @Query(value = "select * from Catalogs where id = 2", nativeQuery = true)
    Catalogs getCatalogs();


    List<Catalogs> findAll();
}
