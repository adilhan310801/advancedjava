package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Search_key_routing;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Search_key_routingRepository extends CrudRepository<Search_key_routing, Long> {


    @Query(value = "select * from Search_key_routing where id = 2", nativeQuery = true)
    Search_key_routing getSearch_key_routing();


    List<Search_key_routing> findAll();
}
