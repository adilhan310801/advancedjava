package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.File_binary;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface File_binaryRepository extends CrudRepository<File_binary, Long> {


    @Query(value = "select * from File_binary where id = 2", nativeQuery = true)
    File_binary getFile_binary();


    List<File_binary> findAll();
}
