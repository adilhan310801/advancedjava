package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Cases;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CasesRepository extends CrudRepository<Cases, Long> {


    @Query(value = "select * from Cases where id = 2", nativeQuery = true)
    Cases getCases();


    List<Cases> findAll();
}
