package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Record;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecordRepository extends CrudRepository<Record, Long> {


    @Query(value = "select * from Record where id = 2", nativeQuery = true)
    Record getRecord();


    List<Record> findAll();
}
