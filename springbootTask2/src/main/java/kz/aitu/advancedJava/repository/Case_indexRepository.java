package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Case_index;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Case_indexRepository extends CrudRepository<Case_index, Long> {


    @Query(value = "select * from Case_index where id = 2", nativeQuery = true)
    Case_index getCase_index();


    List<Case_index> findAll();
}
