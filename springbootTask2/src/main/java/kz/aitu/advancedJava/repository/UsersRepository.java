package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Users;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepository extends CrudRepository<Users, Long> {


    @Query(value = "select * from Users where id = 2", nativeQuery = true)
    Users getUsers();


    List<Users> findAll();
}
