package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Catalog_case;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Catalog_caseRepository extends CrudRepository<Catalog_case, Long> {


    @Query(value = "select * from Catalog_case where id = 2", nativeQuery = true)
    Catalog_case getCatalog_case();


    List<Catalog_case> findAll();
}
