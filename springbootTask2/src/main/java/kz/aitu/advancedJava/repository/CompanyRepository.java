package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Company;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CompanyRepository extends CrudRepository<Company, Long> {


    @Query(value = "select * from Company where id = 2", nativeQuery = true)
    Company getCompany();

    List<Company> findAll();
}
