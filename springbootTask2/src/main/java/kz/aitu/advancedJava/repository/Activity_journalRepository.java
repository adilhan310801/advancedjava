package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Activity_journal;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Activity_journalRepository extends CrudRepository<Activity_journal, Long> {


    @Query(value = "select * from Activity_journal where id = 2", nativeQuery = true)
    Activity_journal getActivity_journal();


    List<Activity_journal> findAll();
}
