package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Company_unit;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Company_unitRepository extends CrudRepository<Company_unit, Long> {


    @Query(value = "select * from Company_unit where id = 2", nativeQuery = true)
    Company_unit getCompany_unit();


    List<Company_unit> findAll();
}
