package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.History_of_request_statuses;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface History_of_request_statusesRepository extends CrudRepository<History_of_request_statuses, Long> {


    @Query(value = "select * from History_of_request_statuses where id = 2", nativeQuery = true)
    History_of_request_statuses getHistory_of_request_statuses();


    List<History_of_request_statuses> findAll();
}
