package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Files;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FilesRepository extends CrudRepository<Files, Long> {


    @Query(value = "select * from Filess where id = 2", nativeQuery = true)
    Files getFiles();


    List<Files> findAll();
}
