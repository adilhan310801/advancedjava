package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Share;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShareRepository extends CrudRepository<Share, Long> {


    @Query(value = "select * from Share where id = 2", nativeQuery = true)
    Share getShare();


    List<Share> findAll();
}
