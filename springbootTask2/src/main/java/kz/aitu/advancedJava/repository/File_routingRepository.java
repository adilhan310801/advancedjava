package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.File_routing;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface File_routingRepository extends CrudRepository<File_routing, Long> {


    @Query(value = "select * from File_routing where id = 2", nativeQuery = true)
    File_routing getFile_routing();


    List<File_routing> findAll();
}
