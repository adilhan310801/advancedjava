package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Nomenclature_summary;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Nomenclature_summaryRepository extends CrudRepository<Nomenclature_summary, Long> {


    @Query(value = "select * from Nomenclature_summary where id = 2", nativeQuery = true)
    Nomenclature_summary getNomenclature_summary();


    List<Nomenclature_summary> findAll();
}
