package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Locations;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationsRepository extends CrudRepository<Locations, Long> {


    @Query(value = "select * from Locations where id = 2", nativeQuery = true)
    Locations getLocations();

    List<Locations> findAll();
}
