package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Authorizations;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorizationsRepository extends CrudRepository<Authorizations, Long> {


    @Query(value = "select * from Authorizations where id = 2", nativeQuery = true)
    Authorizations getAuthorizations();

    List<Authorizations> findAll();
}
