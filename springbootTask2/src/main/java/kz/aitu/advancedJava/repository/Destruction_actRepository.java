package kz.aitu.advancedJava.repository;

import kz.aitu.advancedJava.model.Destruction_act;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Destruction_actRepository extends CrudRepository<Destruction_act, Long> {

    @Query(value = "select * from Destruction_act where id = 2", nativeQuery = true)
    Destruction_act getDestruction_act();


    List<Destruction_act> findAll();
}
