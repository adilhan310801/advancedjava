INSERT INTO public.activity_journal(
    id, event_type, object_type, object_id, created_timestamp, created_by, message_level, message)
VALUES (1,	'for fun',	'Discrete',	1	,20200922,	1,	'high',	'wait u all at activity'),
       (2	,'for pleasure'	,'Discrete',	1	,20200922,	1	,'low','wait u all at activity'),
       (3	,'for sun'	,'Discrete',	1	,20200922	,1	,'medium'	,'wait u all at activity'),
       (4	,'for us',	'Discrete',	1	,20200922,	1	,'high'	,'wait u all at activity'),
       (5	,'for people',	'Discrete'	,1	,20200922,	1	,'high',	'wait u all at activity');


INSERT INTO public.authorizations(
    id, username, email, password, role, forgot_password_key, forgot_password_key_timestamp, company_unit_id)
VALUES (1, 'John', 'Kennedy', 'nottodayplease','president', 'loveumawife', 20200922, 1 ),
       (2, 'Alan', 'Ken', 'qwerty','staff', 'cat', 20200922, 1 ),
       (3, 'Denberk', 'Conan', '123456789','staff', 'dog', 20200922, 1 ),
       (4, 'Johnny', 'Kevin', 'password','staff', 'wife', 20200922, 1 ),
       (5, 'Myrkymbek', 'Shyrkymbek', 'kairatnurtas','staff', 'KAZAKHSTAN', 20200922, 1 );

INSERT INTO public.case_index(
    id, case_index, title_ru, title_kz, title_en, storage_type, storage_year, note, company_unit_id, nomenclature_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1, 'indexA', 'заголовокА', 'тақырыпА', 'titleA', 1, 2020, 'no nodes', 1, 1, 20200922, 1, 20200922, 1),
       (2, 'indexB', 'заголовокB', 'тақырыпB', 'titleB', 1, 2020, 'no nodes', 1, 1, 20200922, 1, 20200922, 1),
       (3, 'indexC', 'заголовокC', 'тақырыпC', 'titleC', 1, 2020, 'no nodes', 1, 1, 20200922, 1, 20200922, 1),
       (4, 'indexD', 'заголовокD', 'тақырыпD', 'titleD', 1, 2020, 'no nodes', 1, 1, 20200922, 1, 20200922, 1),
       (5, 'indexE', 'заголовокE', 'тақырыпE', 'titleE', 1, 2020, 'no nodes', 1, 1, 20200922, 1, 20200922, 1);

INSERT INTO public.cases(
    id, case_number, volume_number, case_title_ru, case_title_kz, case_title_en, start_date, last_date, page_number, signature_flag_eds, signature_eds, nac_sending_sign, delete_sign, limited_access_flag, hash, version, id_version, version_activity_sign, note, id_location, id_case_index, id_record, id_destruction_act, id_company_unit, case_address_blockchain, enter_date_blockchain, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES
(1, '000001A', '1A', 'дело1A', 'іс1А', 'case1A', 20200922, 20200923, 24, true, 'jasbdiugais7diuasday78', true, false, true, '5a15sd1a5s1d53as484', 1, 1, true, 'no note', 1, 1,1,4, 1, 'case_archive', 20200922, 20200922, 1, 20200922, 1),
(2, '000002A', '2A', 'дело2A', 'іс2А', 'case2A', 20200922, 20200923, 24, true, 'ajgsdiaysaoiasudoiauso', true, false, true, '5a1s5d5a1s5d5152222', 2, 1, true, 'no note', 1, 1,2,4, 1, 'case_archive', 20200922, 20200922, 1, 20200922, 1),
(3, '000003A', '3A', 'дело3A', 'іс3А', 'case3A', 20200922, 20200923, 24, true, '172y812y3uih2ek2enkjjk', true, false, true, '9z9xc49z4xc848z11aq', 1, 3, true, 'no note', 3, 1,1,1, 1, 'case_archive', 20200922, 20200922, 1, 20200922, 1),
(4, '000004A', '4A', 'дело4A', 'іс4А', 'case4A', 20200922, 20200923, 24, true, '128y812y3812ye8h28eh98', true, false, true, '8a8sd8a8sd8a8s88888', 2, 3, true, 'no note', 1, 1,2,1, 5, 'case_archive', 20200922, 20200922, 1, 20200922, 1),
(5, '000005A', '5A', 'дело5A', 'іс5А', 'case5A', 20200922, 20200923, 24, true, '1111adiaus98d77a7asdaa', true, false, true, '9oil9io9l9il95l1i51', 1, 4, true, 'no note', 5, 1,1,1, 1, 'case_archive', 20200922, 20200922, 1, 20200922, 1);

INSERT INTO public.catalog_case(
    id, case_id, catalog_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1, 1, 1, 1, 20200922, 1, 20200922, 1),
       (2, 2, 1, 1, 20200922, 1, 20200922, 1),
       (3, 3, 1, 1,  20200922, 1, 20200922, 1),
       (4, 4, 1, 1,  20200922, 1, 20200922, 1),
       (5, 5, 1, 1,  20200922, 1, 20200922, 1);

INSERT INTO public.catalogs(
    id, name_ru, name_kz, name_en, parent_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1, 'каталогA', 'каталогА', 'catalogA', 1, 1,  20200922, 1, 20200922, 1),
       (2, 'каталогB', 'каталогB', 'catalogB', 1, 2,  20200922, 1, 20200922, 1),
       (3, 'каталогC', 'каталогC', 'catalogC', 2, 1,  20200922, 1, 20200922, 1),
       (4, 'каталогD', 'каталогD', 'catalogD', 1, 3,  20200922, 1, 20200922, 1),
       (5, 'каталогE', 'каталогE', 'catalogE', 3, 1,  20200922, 1, 20200922, 1);

INSERT INTO public.company(
    id, name_ru, name_kz, name_en, bin, parent_id, fond_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1, 'Алтай','Алтай' , 'Altai', '12052555', 1, 1, 20200922, 1, 20200922, 1),
       (2, 'Батыр','Батыр' , 'Batyr', '12052556', 1, 1, 20200922, 1, 20200922, 1),
       (3, 'Залог','Залог' , 'Zalog', '12052557', 1, 1, 20200922, 1, 20200922, 1),
       (4, 'Такия','Тақия' , 'Takiya', '12052558', 1, 1, 20200922, 1, 20200922, 1),
       (5, 'Сулу','Сұлу' , 'Sulu', '12052559', 1, 1, 20200922, 1, 20200922, 1);


INSERT INTO public.company_unit(
    id, name_ru, name_kz, name_en, parent_id, year, company_id, code_index, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1, 'Алтай','Алтай' , 'Altai',1,2020,1,1, 20200922, 1, 20200922, 1),
       (2, 'Батыр','Батыр' , 'Batyr',1,2020,2, 1, 20200922, 1, 20200922, 1),
       (3, 'Залог','Залог' , 'Zalog',1,2020,3,  1, 20200922, 1, 20200922, 1),
       (4, 'Такия','Тақия' , 'Takiya',1,2020,4, 1, 20200922, 1, 20200922, 1),
       (5, 'Сулу','Сұлу' , 'Sulu',1,2020,5, 1, 20200922, 1, 20200922, 1);


INSERT INTO public.destruction_act(
    id, act_number, basis, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1, '1', 'basisA', 1, 20200922, 1, 20200922, 1),
       (2, '2', 'basisB', 2, 20200922, 1, 20200922, 1),
       (3, '3', 'basisC', 3, 20200922, 1, 20200922, 1),
       (4, '4', 'basisD', 4, 20200922, 1, 20200922, 1),
       (5, '5', 'basisE', 5, 20200922, 1, 20200922, 1);


INSERT INTO public.file_binary(
    id, file_binary, file_binary_byte)
VALUES (1, 'file_binaryA', '/000'),
       (2, 'file_binaryB', '/001'),
       (3, 'file_binaryC', '/010'),
       (4, 'file_binaryD', '/011'),
       (5, 'file_binaryE', '/100');


INSERT INTO public.file_routing(
    id, file_id, table_name, table_id, type)
VALUES (1, 1, 'tableA', 1, 'typeA'),
       (2, 2, 'tableB', 2, 'typeB'),
       (3, 3, 'tableC', 3, 'typeC'),
       (4, 4, 'tableD', 4, 'typeD'),
       (5,5, 'tableE', 5, 'typeE');

INSERT INTO public.files(
    id, name, type, size, page_count, hash, is_deleted, file_binary_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1, 'nameA', 'typeA',255, '25', 'ajsda76879qwjdaas', false, 1,20200922, 1, 20200922, 1),
       (2, 'nameB', 'typeB',255, '25', '78asd8asandjasdas', false, 1,20200922, 1, 20200922, 1),
       (3, 'nameC', 'typeC',255, '25', '90asdckaosd999a9s', false, 1,20200922, 1, 20200922, 1),
       (4, 'nameD', 'typeD',255, '25', '23456a7shdnjasnaa', false, 1,20200922, 1, 20200922, 1),
       (5, 'nameE', 'typeE',255, '25', '23zx4c56zx8cz9xcc', false, 1,20200922, 1, 20200922, 1);

INSERT INTO public.fond(
    id, fond_number, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1, '255', 20200922, 1, 20200922, 1),
       (2, '355', 20200922, 1, 20200922, 1),
       (3, '455', 20200922, 1, 20200922, 1),
       (4, '955', 20200922, 1, 20200922, 1),
       (5, '1055', 20200922, 1, 20200922, 1);

INSERT INTO public.history_of_request_statuses(
    id, request_id, status, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1, 1, 'statusA', 20200922, 1, 20200922, 1),
       (2, 2, 'statusB', 20200922, 1, 20200922, 1),
       (3, 3, 'statusC', 20200922, 1, 20200922, 1),
       (4, 4, 'statusD', 20200922, 1, 20200922, 1),
       (5, 5, 'statusE', 20200922, 1, 20200922, 1);


INSERT INTO public.locations(
    id, "row", line, "column", box, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1, '1A', '1A', '1A', '1A', 1, 20200922, 1, 20200922, 1),
       (2, '2A', '2A', '2A', '2A', 1, 20200922, 1, 20200922, 2),
       (3, '3A', '3A', '3A', '3A', 1, 20200922, 1, 20200922, 3),
       (4, '4A', '4A', '4A', '4A', 1, 20200922, 1, 20200922, 4),
       (5, '5A', '5A', '5A', '5A', 1, 20200922, 1, 20200922, 5);



INSERT INTO public.nomenclature(
    id, nomenclature_number, year, nomenclature_summary_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1, '123', 2020, 1, 1, 20200922, 1, 20200922, 1),
       (2, '1234', 2020, 2, 1, 20200922, 1, 20200922, 1),
       (3, '1235', 2020, 3, 2, 20200922, 1, 20200922, 1),
       (4, '1236', 2020, 4, 4, 20200922, 1, 20200922, 1),
       (5, '1237', 2020, 5, 3, 20200922, 1, 20200922, 1);


INSERT INTO public.nomenclature_summary(
    id, "number", year, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1, '123', 2020, 1, 20200922, 1, 20200922, 1),
       (2, '1234', 2020,  1, 20200922, 1, 20200922, 1),
       (3, '1235', 2020,  2, 20200922, 1, 20200922, 1),
       (4, '1236', 2020, 4, 20200922, 1, 20200922, 1),
       (5, '1237', 2020, 3, 20200922, 1, 20200922, 1);

INSERT INTO public.notification(
    id, object_type, object_id, company_unit_id, user_id, created_timestamp, viewed_timestamp, is_viewed, title, text, company_id)
VALUES (1, 'otypeA', 1, 1, 2, 20200922, 20200922021723, true, 'titleA', 'textA', 1),
       (2, 'otypeB', 2, 2, 1, 20200922, 20200922022801, true, 'titleB', 'textB', 2),
       (3, 'otypeC', 1, 1, 3, 20200922, 20200922020259, false, 'titleC', 'textC', 3),
       (4, 'otypeD', 3, 4, 5, 20200922, 20200922021755, true, 'titleD', 'textD', 4),
       (5, 'otypeE', 1, 1, 4, 20200922, 20200922021813, false, 'titleE', 'textE', 5);


INSERT INTO public.record(
    id, "number", type, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1, '221', 'typeA', 4, 20200922, 1, 20200922, 1),
       (2, '223', 'typeB', 2, 20200922, 1, 20200922, 1),
       (3, '225', 'typeC', 1, 20200922, 1, 20200922, 1),
       (4, '227', 'typeD', 3, 20200922, 1, 20200922, 1),
       (5, '228', 'typeE', 5, 20200922, 1, 20200922, 1);


INSERT INTO public.request(
    id, request_user_id, responce_user_id, case_id, case_index_id, created_type, comment, status, "timestamp", sharestart, sharefinish, favorite, update_timestamp, update_by, declinenote, company_unit_id, from_request_id)
VALUES (1, 1, 1, 1, 1, 'typeA', 'comment', 'nice', 20200922,20200922,20200923 , true,  20200922, 1, 'dnote', 1, 2),
       (2, 1, 1, 1, 1, 'typeB', 'comment', 'nice', 20200922,20200922,20200923 , true,  20200922, 1, 'dnote', 1, 3),
       (3, 1, 1, 1, 1, 'typeC', 'comment', 'nice', 20200922,20200922,20200923 , true,  20200922, 1, 'dnote', 1, 4),
       (4, 1, 1, 1, 1, 'typeD', 'comment', 'nice', 20200922,20200922,20200923 , true,  20200922, 1, 'dnote', 1, 5),
       (5, 1, 1, 1, 1, 'typeE', 'comment', 'nice', 20200922,20200922,20200923 , true,  20200922, 1, 'dnote', 1, 1);


INSERT INTO public.search_key_routing(
    id, search_key_id, table_name, table_id, type)
VALUES (1, 3, 'tableA', 1, 'typeA'),
       (2, 4, 'tableB', 2, 'typeB'),
       (3, 5, 'tableC', 3, 'typeC'),
       (4, 2, 'tableD', 4, 'typeD'),
       (5, 1, 'tableE', 5, 'typeE');



INSERT INTO public.searchkey(
    id, name, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1, 'key1', 1, 20200922, 1, 20200922, 1),
       (2, 'key2', 1, 20200922, 1, 20200922, 1),
       (3, 'key3', 2, 20200922, 1, 20200922, 1),
       (4, 'key4', 1, 20200922, 1, 20200922, 1),
       (5, 'key5', 1, 20200922, 1, 20200922, 1);


INSERT INTO public.share(
    id, request_id, note, sender_id, receiver_id, share_timestamp)
VALUES (1, 5, 'note', 1, 2, 20200922),
       (2, 4, 'note', 2, 3, 20200922),
       (3, 3, 'note', 3, 4, 20200922),
       (4, 2, 'note', 4, 5, 20200922),
       (5, 1, 'note', 5, 1, 20200922);



INSERT INTO public.users(
    id, auth_id, name, fullname, surname, secondname, status, company_unit_id, password, last_login_timestamp, iin, is_active, is_activated, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1, 1, 'John', 'John Kennedy', 'Kennedy', 'Kennedyuly', 'nice', 1, 'president', 20200922, 5620158488521, false, true, 20200922, 1, 20200922, 1),
       (2, 2, 'Alan', 'Alan Ken', 'Ken', 'Kenuly', 'nice', 2, 'password', 20200922, 820222156985, false, true, 20200922, 1, 20200922, 1),
       (3, 3, 'Denberk', 'Denberk Conan', 'Conan', 'Conanuly', 'nice', 3, 'verystrogpasswordever', 20200922, 990201025484, false, true, 20200922, 1, 20200922, 1),
       (4, 4, 'Johnny', 'John Kevin', 'Kevin', 'Kevinuly', 'nice', 4, 'hihacker', 20200922, 760825148566, false, true, 20200922, 1, 20200922, 1),
       (5, 5, 'Myrkymbek', 'Myrkymbek Shyrkymbek', 'Shyrkymber', 'Shyrkymbekuly', 'nice', 5, '@a@a@a', 20200922, 810225695844, false, true, 20200922, 1, 20200922, 1);


















