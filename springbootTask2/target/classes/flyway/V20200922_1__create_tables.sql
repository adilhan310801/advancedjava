CREATE TABLE public.activity_journal
(
    id bigint NOT NULL,
    event_type character varying(128) COLLATE pg_catalog."default" NOT NULL,
    object_type character varying(255) COLLATE pg_catalog."default" NOT NULL,
    object_id bigint NOT NULL,
    created_timestamp bigint NOT NULL,
    created_by bigint NOT NULL,
    message_level character varying(128) COLLATE pg_catalog."default" NOT NULL,
    message character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "Activity_journal_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.activity_journal
    OWNER to postgres;




CREATE TABLE public.authorizations
(
    id bigint NOT NULL,
    username character varying(255) COLLATE pg_catalog."default" NOT NULL,
    email character varying(255) COLLATE pg_catalog."default" NOT NULL,
    password character varying(128) COLLATE pg_catalog."default" NOT NULL,
    role character varying(255) COLLATE pg_catalog."default" NOT NULL,
    forgot_password_key character varying(128) COLLATE pg_catalog."default" NOT NULL,
    forgot_password_key_timestamp bigint NOT NULL,
    company_unit_id bigint NOT NULL,
    CONSTRAINT "Authorization_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.authorizations
    OWNER to postgres;



CREATE TABLE public.cases
(
    id bigint NOT NULL,
    case_number character varying(128) COLLATE pg_catalog."default" NOT NULL,
    volume_number character varying(128) COLLATE pg_catalog."default" NOT NULL,
    case_title_ru character varying(128) COLLATE pg_catalog."default" NOT NULL,
    case_title_kz character varying(128) COLLATE pg_catalog."default" NOT NULL,
    case_title_en character varying(128) COLLATE pg_catalog."default" NOT NULL,
    start_date bigint NOT NULL,
    last_date bigint NOT NULL,
    page_number bigint NOT NULL,
    signature_flag_eds boolean NOT NULL,
    signature_eds text COLLATE pg_catalog."default" NOT NULL,
    nac_sending_sign boolean NOT NULL,
    delete_sign boolean NOT NULL,
    limited_access_flag boolean NOT NULL,
    hash character varying(128) COLLATE pg_catalog."default" NOT NULL,
    version bigint NOT NULL,
    id_version character varying(128) COLLATE pg_catalog."default" NOT NULL,
    version_activity_sign boolean NOT NULL,
    note character varying(255) COLLATE pg_catalog."default" NOT NULL,
    id_location bigint NOT NULL,
    id_case_index bigint NOT NULL,
    id_record bigint NOT NULL,
    id_destruction_act bigint,
    id_company_unit bigint,
    case_address_blockchain character varying(128) COLLATE pg_catalog."default",
    enter_date_blockchain bigint,
    created_timestamp bigint,
    created_by bigint,
    updated_timestamp bigint,
    updated_by bigint,
    CONSTRAINT "Case_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.cases
    OWNER to postgres;


CREATE TABLE public.case_index
(
    id bigint NOT NULL,
    case_index character varying(128) COLLATE pg_catalog."default" NOT NULL,
    title_ru character varying(128) COLLATE pg_catalog."default" NOT NULL,
    title_kz character varying(128) COLLATE pg_catalog."default" NOT NULL,
    title_en character varying(128) COLLATE pg_catalog."default" NOT NULL,
    storage_type bigint NOT NULL,
    storage_year bigint NOT NULL,
    note character varying(128) COLLATE pg_catalog."default" NOT NULL,
    company_unit_id bigint NOT NULL,
    nomenclature_id bigint NOT NULL,
    created_timestamp bigint NOT NULL,
    created_by bigint NOT NULL,
    updated_timestamp bigint NOT NULL,
    updated_by bigint NOT NULL,
    CONSTRAINT "Case_index_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.case_index
    OWNER to postgres;


CREATE TABLE public.catalogs
(
    id bigint NOT NULL,
    name_ru character varying(128) COLLATE pg_catalog."default" NOT NULL,
    name_kz character varying(128) COLLATE pg_catalog."default" NOT NULL,
    name_en character varying(128) COLLATE pg_catalog."default" NOT NULL,
    parent_id bigint NOT NULL,
    company_unit_id bigint NOT NULL,
    created_timestamp bigint NOT NULL,
    created_by bigint NOT NULL,
    updated_timestamp bigint NOT NULL,
    updated_by bigint NOT NULL,
    CONSTRAINT "Catalog_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.catalogs
    OWNER to postgres;


CREATE TABLE public.catalog_case
(
    id bigint NOT NULL,
    case_id bigint NOT NULL,
    catalog_id bigint NOT NULL,
    company_unit_id bigint NOT NULL,
    created_timestamp bigint NOT NULL,
    created_by bigint NOT NULL,
    updated_timestamp bigint NOT NULL,
    updated_by bigint NOT NULL,
    CONSTRAINT "Catalog_case_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.catalog_case
    OWNER to postgres;



CREATE TABLE public.company
(
    id bigint NOT NULL,
    name_ru character varying(128) COLLATE pg_catalog."default" NOT NULL,
    name_kz character varying(128) COLLATE pg_catalog."default" NOT NULL,
    name_en character varying(128) COLLATE pg_catalog."default" NOT NULL,
    bin character varying(32) COLLATE pg_catalog."default" NOT NULL,
    parent_id bigint NOT NULL,
    fond_id bigint NOT NULL,
    created_timestamp bigint NOT NULL,
    created_by bigint NOT NULL,
    updated_timestamp bigint NOT NULL,
    updated_by bigint NOT NULL,
    CONSTRAINT "Company_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.company
    OWNER to postgres;



CREATE TABLE public.company_unit
(
    id bigint NOT NULL,
    name_ru character varying(128) COLLATE pg_catalog."default" NOT NULL,
    name_kz character varying(128) COLLATE pg_catalog."default" NOT NULL,
    name_en character varying(128) COLLATE pg_catalog."default" NOT NULL,
    parent_id bigint NOT NULL,
    year bigint NOT NULL,
    company_id bigint NOT NULL,
    code_index character varying(16) COLLATE pg_catalog."default" NOT NULL,
    created_timestamp bigint NOT NULL,
    created_by bigint NOT NULL,
    updated_timestamp bigint NOT NULL,
    updated_by bigint NOT NULL,
    CONSTRAINT "Company_unit_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.company_unit
    OWNER to postgres;



CREATE TABLE public.destruction_act
(
    id bigint NOT NULL,
    act_number character varying(128) COLLATE pg_catalog."default" NOT NULL,
    basis character varying(256) COLLATE pg_catalog."default" NOT NULL,
    company_unit_id bigint NOT NULL,
    created_timestamp bigint NOT NULL,
    created_by bigint NOT NULL,
    updated_timestamp bigint NOT NULL,
    updated_by bigint NOT NULL,
    CONSTRAINT "Destruction_act_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.destruction_act
    OWNER to postgres;



CREATE TABLE public.files
(
    id bigint NOT NULL,
    name character varying(128) COLLATE pg_catalog."default" NOT NULL,
    type character varying(128) COLLATE pg_catalog."default" NOT NULL,
    size bigint NOT NULL,
    page_count bigint NOT NULL,
    hash character varying(128) COLLATE pg_catalog."default" NOT NULL,
    is_deleted boolean NOT NULL,
    file_binary_id bigint NOT NULL,
    created_timestamp bigint NOT NULL,
    created_by bigint NOT NULL,
    updated_timestamp bigint NOT NULL,
    updated_by bigint NOT NULL,
    CONSTRAINT "File_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.files
    OWNER to postgres;




CREATE TABLE public.file_binary
(
    id bigint NOT NULL,
    file_binary text COLLATE pg_catalog."default" NOT NULL,
    file_binary_byte bytea NOT NULL,
    CONSTRAINT "File_binary_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.file_binary
    OWNER to postgres;


CREATE TABLE public.file_routing
(
    id bigint NOT NULL,
    file_id bigint NOT NULL,
    table_name character varying(128) COLLATE pg_catalog."default" NOT NULL,
    table_id bigint NOT NULL,
    type character varying(128) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "File_routing_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.file_routing
    OWNER to postgres;

CREATE TABLE public.fond
(
    id bigint NOT NULL,
    fond_number character varying(128) COLLATE pg_catalog."default" NOT NULL,
    created_timestamp bigint NOT NULL,
    created_by bigint NOT NULL,
    updated_timestamp bigint NOT NULL,
    updated_by bigint NOT NULL,
    CONSTRAINT "Fond_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.fond
    OWNER to postgres;



CREATE TABLE public.history_of_request_statuses
(
    id bigint NOT NULL,
    request_id bigint NOT NULL,
    status character varying(64) COLLATE pg_catalog."default" NOT NULL,
    created_timestamp bigint NOT NULL,
    created_by bigint NOT NULL,
    updated_timestamp bigint NOT NULL,
    updated_by bigint NOT NULL,
    CONSTRAINT " History_of_request_statuses_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.history_of_request_statuses
    OWNER to postgres;



CREATE TABLE public.locations
(
    id bigint NOT NULL,
    "row" character varying(64) COLLATE pg_catalog."default" NOT NULL,
    line character varying(64) COLLATE pg_catalog."default" NOT NULL,
    "column" character varying(64) COLLATE pg_catalog."default" NOT NULL,
    box character varying(64) COLLATE pg_catalog."default" NOT NULL,
    company_unit_id bigint NOT NULL,
    created_timestamp bigint NOT NULL,
    created_by bigint NOT NULL,
    updated_timestamp bigint NOT NULL,
    updated_by bigint NOT NULL,
    CONSTRAINT "Location_pkey" PRIMARY KEY (updated_by)
)

    TABLESPACE pg_default;

ALTER TABLE public.locations
    OWNER to postgres;



CREATE TABLE public.nomenclature
(
    id bigint NOT NULL,
    nomenclature_number character varying(128) COLLATE pg_catalog."default" NOT NULL,
    year bigint NOT NULL,
    nomenclature_summary_id bigint NOT NULL,
    company_unit_id bigint NOT NULL,
    created_timestamp bigint NOT NULL,
    created_by bigint NOT NULL,
    updated_timestamp bigint NOT NULL,
    updated_by bigint NOT NULL,
    CONSTRAINT "Nomenclature_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.nomenclature
    OWNER to postgres;



CREATE TABLE public.nomenclature_summary
(
    id bigint NOT NULL,
    "number" character varying(128) COLLATE pg_catalog."default" NOT NULL,
    year bigint NOT NULL,
    company_unit_id bigint NOT NULL,
    created_timestamp bigint NOT NULL,
    created_by bigint NOT NULL,
    updated_timestamp bigint NOT NULL,
    updated_by bigint NOT NULL,
    CONSTRAINT "Nomenclature_summary_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.nomenclature_summary
    OWNER to postgres;



CREATE TABLE public.notification
(
    id bigint NOT NULL,
    object_type character varying(128) COLLATE pg_catalog."default" NOT NULL,
    object_id bigint NOT NULL,
    company_unit_id bigint NOT NULL,
    user_id bigint NOT NULL,
    created_timestamp bigint NOT NULL,
    viewed_timestamp bigint NOT NULL,
    is_viewed boolean NOT NULL,
    title character varying(255) COLLATE pg_catalog."default" NOT NULL,
    text character varying(255) COLLATE pg_catalog."default" NOT NULL,
    company_id bigint NOT NULL,
    CONSTRAINT "Notification_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.notification
    OWNER to postgres;




CREATE TABLE public.record
(
    id bigint NOT NULL,
    "number" character varying(128) COLLATE pg_catalog."default" NOT NULL,
    type character varying(128) COLLATE pg_catalog."default" NOT NULL,
    company_unit_id bigint NOT NULL,
    created_timestamp bigint NOT NULL,
    created_by bigint NOT NULL,
    updated_timestamp bigint NOT NULL,
    updated_by bigint NOT NULL,
    CONSTRAINT "Record_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.record
    OWNER to postgres;



CREATE TABLE public.request
(
    id bigint NOT NULL,
    request_user_id bigint NOT NULL,
    responce_user_id bigint NOT NULL,
    case_id bigint NOT NULL,
    case_index_id bigint NOT NULL,
    created_type character varying(64) COLLATE pg_catalog."default" NOT NULL,
    comment character varying(255) COLLATE pg_catalog."default" NOT NULL,
    status character varying(64) COLLATE pg_catalog."default" NOT NULL,
    "timestamp" bigint NOT NULL,
    sharestart bigint NOT NULL,
    sharefinish bigint NOT NULL,
    favorite boolean NOT NULL,
    update_timestamp bigint NOT NULL,
    update_by bigint NOT NULL,
    declinenote character varying(255) COLLATE pg_catalog."default" NOT NULL,
    company_unit_id bigint NOT NULL,
    from_request_id bigint NOT NULL,
    CONSTRAINT "Request_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.request
    OWNER to postgres;



CREATE TABLE public.search_key_routing
(
    id bigint NOT NULL,
    search_key_id bigint NOT NULL,
    table_name character varying(128) COLLATE pg_catalog."default" NOT NULL,
    table_id bigint NOT NULL,
    type character varying(128) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "Search_key_routing_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.search_key_routing
    OWNER to postgres;


CREATE TABLE public.searchkey
(
    id bigint NOT NULL,
    name character varying(128) COLLATE pg_catalog."default" NOT NULL,
    company_unit_id bigint NOT NULL,
    created_timestamp bigint NOT NULL,
    created_by bigint NOT NULL,
    updated_timestamp bigint NOT NULL,
    updated_by bigint NOT NULL,
    CONSTRAINT "Searchkey_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.searchkey
    OWNER to postgres;


CREATE TABLE public.share
(
    id bigint NOT NULL,
    request_id bigint NOT NULL,
    note character varying(255) COLLATE pg_catalog."default" NOT NULL,
    sender_id bigint NOT NULL,
    receiver_id bigint NOT NULL,
    share_timestamp bigint NOT NULL,
    CONSTRAINT "Share_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.share
    OWNER to postgres;


CREATE TABLE public.users
(
    id bigint NOT NULL,
    auth_id bigint NOT NULL,
    name character varying(128) COLLATE pg_catalog."default" NOT NULL,
    fullname character varying(128) COLLATE pg_catalog."default" NOT NULL,
    surname character varying(128) COLLATE pg_catalog."default" NOT NULL,
    secondname character varying(128) COLLATE pg_catalog."default" NOT NULL,
    status character varying(128) COLLATE pg_catalog."default" NOT NULL,
    company_unit_id bigint NOT NULL,
    password character varying(128) COLLATE pg_catalog."default" NOT NULL,
    last_login_timestamp bigint NOT NULL,
    iin character varying(32) COLLATE pg_catalog."default" NOT NULL,
    is_active boolean NOT NULL,
    is_activated boolean NOT NULL,
    created_timestamp bigint NOT NULL,
    created_by bigint NOT NULL,
    updated_timestamp bigint NOT NULL,
    updated_by bigint NOT NULL,
    CONSTRAINT "Users_pkey" PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.users
    OWNER to postgres;