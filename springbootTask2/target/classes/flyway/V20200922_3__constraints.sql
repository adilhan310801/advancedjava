
ALTER TABLE Users
    ADD CONSTRAINT users_fkey1 FOREIGN KEY (auth_id) REFERENCES Authorizations(id);

ALTER TABLE company_unit
    ADD CONSTRAINT company_unit_fkey1 FOREIGN KEY (company_id) REFERENCES company(id);

ALTER TABLE company
    ADD CONSTRAINT company_fkey1 FOREIGN KEY (fond_id) REFERENCES fond(id);

ALTER TABLE record
    ADD CONSTRAINT record_fkey1 FOREIGN KEY (company_unit_id) REFERENCES company_unit(id);

ALTER TABLE share
    ADD CONSTRAINT share_fkey1 FOREIGN KEY (request_id) REFERENCES request(id);

ALTER TABLE history_of_request_statuses
    ADD CONSTRAINT history_of_request_statuses_fkey1 FOREIGN KEY (request_id) REFERENCES request(id);

ALTER TABLE catalog_case
    ADD CONSTRAINT catalog_case_fkey1 FOREIGN KEY (catalog_id) REFERENCES catalogs(id);



ALTER TABLE catalog_case
    ADD CONSTRAINT catalog_case_fkey2 FOREIGN KEY (case_id) REFERENCES cases(id);

ALTER TABLE catalog_case
    ADD CONSTRAINT catalog_case_fkey3 FOREIGN KEY (company_unit_id) REFERENCES company_unit(id);

ALTER TABLE file_routing
    ADD CONSTRAINT file_routing_fkey1 FOREIGN KEY (table_id) REFERENCES cases(id);

ALTER TABLE request
    ADD CONSTRAINT request_fkey1 FOREIGN KEY (case_id) REFERENCES cases(id);

ALTER TABLE request
    ADD CONSTRAINT request_fkey2 FOREIGN KEY (case_index_id) REFERENCES case_index(id);

ALTER TABLE file_routing
    ADD CONSTRAINT file_routing_fkey2 FOREIGN KEY (file_id) REFERENCES files(id);

ALTER TABLE files
    ADD CONSTRAINT file_fkey1 FOREIGN KEY (file_binary_id) REFERENCES file_binary(id);

ALTER TABLE cases
    ADD CONSTRAINT cases_fkey2 FOREIGN KEY (id_company_unit) REFERENCES company_unit(id);

ALTER TABLE cases
    ADD CONSTRAINT cases_fkey3 FOREIGN KEY (id_destruction_act) REFERENCES destruction_act(id);

ALTER TABLE cases
    ADD CONSTRAINT cases_fkey4 FOREIGN KEY (id_record) REFERENCES record(id);

ALTER TABLE cases
    ADD CONSTRAINT cases_fkey5 FOREIGN KEY (id_case_index) REFERENCES case_index(id);

ALTER TABLE nomenclature_summary
    ADD CONSTRAINT nomenclature_summary_fkey1 FOREIGN KEY (company_unit_id) REFERENCES company_unit(id);

ALTER TABLE users
    ADD CONSTRAINT users_fkey2 FOREIGN KEY (company_unit_id) REFERENCES company_unit(id);

ALTER TABLE request
    ADD CONSTRAINT request_fkey3 FOREIGN KEY (company_unit_id) REFERENCES company_unit(id);

ALTER TABLE nomenclature
    ADD CONSTRAINT fkey1 FOREIGN KEY (company_unit_id) REFERENCES company_unit(id);

ALTER TABLE searchkey
    ADD CONSTRAINT searchkey_fkey1 FOREIGN KEY (company_unit_id) REFERENCES company_unit(id);

ALTER TABLE search_key_routing
    ADD CONSTRAINT search_key_routing_fkey1 FOREIGN KEY (search_key_id) REFERENCES searchkey(id);

ALTER TABLE search_key_routing
    ADD CONSTRAINT search_key_routing_fkey2 FOREIGN KEY (table_id) REFERENCES cases(id);

ALTER TABLE destruction_act
    ADD CONSTRAINT destruction_act_fkey1 FOREIGN KEY (company_unit_id) REFERENCES company_unit(id);

ALTER TABLE case_index
    ADD CONSTRAINT case_index_fkey1 FOREIGN KEY (company_unit_id) REFERENCES company_unit(id);