CREATE TABLE public."Group"
(
    id bigint NOT NULL,
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT "Group_pkey" PRIMARY KEY (id)
)

CREATE TABLE public."Student"
(
    id bigint NOT NULL,
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    phone character varying(255) COLLATE pg_catalog."default" NOT NULL,
    "groupId" bigint NOT NULL,
    CONSTRAINT "Student_pkey" PRIMARY KEY (id),
    CONSTRAINT "Student_groupId_fkey" FOREIGN KEY ("groupId")
        REFERENCES public."Group" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

